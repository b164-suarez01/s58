import logo from './logo.svg';
import './App.css';

//remove the template in the module, to give way to the new line of code that 
//when compiling multiple components, they should be wrapped  inside a single element


//PUBLIC -. to store all document and modules that are shared across all components og the app that are visible to all. To acquire the image, must get it from the public folder first (No need na mag./public/name)
import cartoon from './PatrickStar.png';
  //pass down the alias that identifies the resource into our element

function App() {
  return (
  <div>
    <h1>Welcome to the course booking batch 164</h1>
    <h5>This is our project in React</h5>
    <h6>Come Visit our website</h6>
   {/*Using image from non_Public folder*/} 
    <img src={cartoon} alt ="image not found"/>
   {/*Using image from Public folder*/}
    <img src="/PatrickStar.png"/>

    <h3>This is my Favorite Cartoon Character</h3>

  </div>
  );
}

export default App;
